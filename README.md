# ON.TO take home task

## Pre-requisites
- Install terraform: https://learn.hashicorp.com/tutorials/terraform/install-cli

## Configuration
- Amend variables in `.secrets.tfvars.example` and rename to `secrets.tfvars` - include your own IP to copy this e.g. `curl ifconfig.me | xclip`
- choose aws_region and set it in `variables.tf`

## Create infrastructure
- `terraform apply -var-file="secrets.tfvars"`

## Test system
- The EC2 node can be used as a bastion to connect to RDS - public key is shared via the aws CLI using your iam credentials (your user will need permissions for ssm for this)
- connect to the host using the following string which will securely connect using iam and terraform outputs from previously created resources: 
```
export AWS_REGION=$(terraform output aws_region | sed s/"\""/""/g) \
&& aws ec2-instance-connect send-ssh-public-key \
--instance-id $(terraform output ec2_instance_id | sed s/"\""/""/g) \
--availability-zone $(terraform output ec2_availability_zone | sed s/"\""/""/g) \
--instance-os-user ec2-user --ssh-public-key file://~/.ssh/id_rsa.pub \
&& ssh ec2-user@$(terraform output ec2_public_ip | sed s/"\""/""/g)
```
- the instance comes with mariadb preinstalled and the internal dns for the rds instance loaded as an environment variable, so you can connect to the db on the cli using:
```
mysql -h $RDS_DNS -P 3306 -u admin -p mydb
``` 
(for -u and password Use credentials you created in `secrets.tfvars`)

## Thoughts on later questions:

- cost economization: single NAT gateway, EC2 spot instances (could also leverage autoscaling if workloads deployed horizontally), use ARM chip for the box and graviton instance, and smallest possible rds size for workload (autoscaling too with aurora)
- secure - ACLs blocking everything on subnets, and have the rds instance only able to communicate with the ec2 and vice versa, remove ec2-instance-connect, bring ec2 into private network and stick it behind reverse proxy
- highly scalable - autoscale on the Ec2 for number of reqs/second/saturation - use aurora for autoscaling