#------------------------------------------------------------------------------
# Terraform Config
#------------------------------------------------------------------------------
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0.0"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  profile = "default"
  region  = var.aws_region
}

#------------------------------------------------------------------------------
# VPC and Security groups
#------------------------------------------------------------------------------

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "tf-vpc-onto"
  cidr = "10.2.0.0/16"

  azs             = ["${var.aws_region}a", "${var.aws_region}b", "${var.aws_region}c"]
  private_subnets = ["10.2.0.0/24", "10.2.1.0/24", "10.2.2.0/24"]
  public_subnets  = ["10.2.100.0/24", "10.2.101.0/24", "10.2.102.0/24"]

  enable_nat_gateway = true
  single_nat_gateway = true # cost economisation

  tags = {
    Terraform   = "true"
    Environment = "onto"
  }
}

resource "aws_security_group" "ec2_instance" {
  name        = "tf_ssh"
  description = "Allow ssh inbound traffic"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "Allow SSH from my computer"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.my_ip}/32"]
  }

  egress { # could have the ec2 only talk to the db
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Terraform   = "true"
    Environment = "onto"
  }
}

resource "aws_security_group" "rds_access" {
  name        = "tf_rds_sg"
  description = "sg for rds db"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description     = "Allow sql traffic"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.ec2_instance.id]
  }

  tags = {
    Terraform   = "true"
    Environment = "onto"
  }
}


#------------------------------------------------------------------------------
# IAM for EC2 instance
#------------------------------------------------------------------------------

resource "aws_iam_role" "instance_connect" {
  name        = "instance-connect"
  description = "privileges for the instance-connect"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": ["ec2.amazonaws.com", "ssm.amazonaws.com" ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "instance_connect" {
  role       = aws_iam_role.instance_connect.id
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}

resource "aws_iam_instance_profile" "instance_connect" {
  name = "instance-connect"
  role = aws_iam_role.instance_connect.id
  tags = {
    Terraform   = "true"
    Environment = "onto"
  }
}

#------------------------------------------------------------------------------
# EC2 instance
#------------------------------------------------------------------------------

data "aws_ami" "amazon-linux-2" {
  most_recent = true


  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }


  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }

  owners = ["amazon"]
}

resource "aws_instance" "bastion" {
  ami                         = data.aws_ami.amazon-linux-2.id
  instance_type               = "t3.micro"
  user_data                   = "#!/bin/bash\nsudo yum -y install ec2-instance-connect mariadb && echo 'export RDS_DNS=${aws_db_instance.instance.address}' >> /home/ec2-user/.bash_profile"
  subnet_id                   = module.vpc.public_subnets.0
  associate_public_ip_address = true
  iam_instance_profile        = aws_iam_instance_profile.instance_connect.name
  security_groups             = [aws_security_group.ec2_instance.id]

  tags = {
    Terraform   = "true"
    Environment = "onto"
  }

  lifecycle {
    ignore_changes = [security_groups]
  }
}

#------------------------------------------------------------------------------
# RDS
#------------------------------------------------------------------------------

resource "aws_db_subnet_group" "rds_subnet_group" {
  name       = "tf_rds_subnet_group"
  subnet_ids = [for subnet in module.vpc.private_subnets : subnet]
}

resource "aws_db_instance" "instance" {
  allocated_storage      = 10
  db_subnet_group_name   = aws_db_subnet_group.rds_subnet_group.id
  vpc_security_group_ids = [aws_security_group.rds_access.id]
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = "db.t3.micro"
  db_name                = "mydb"
  username               = var.db_user
  password               = var.db_password
  parameter_group_name   = "default.mysql5.7"
  skip_final_snapshot    = true

  tags = {
    Terraform   = "true"
    Environment = "onto"
  }
}

# cost economization: single NAT gateway, EC2 spot instances (autoscaling), use ARM chip for the box and g instance, and smallest possible rds size for workload (autoscaling)
# secure - ACLs blocking everything on subnets, and have the rds instance only able to communicate with the ec2 and vice versa, remove ec2-instance-connect, bring ec2 into private network
# highly scalable - autoscale on the Ec2 for number of reqs/second/saturation - use aurora for autoscaling
