output "ec2_public_ip" {
  value = aws_instance.bastion.public_ip
}

output "ec2_instance_id" {
  value = aws_instance.bastion.id
}

output "ec2_availability_zone" {
  value = aws_instance.bastion.availability_zone
}

output "aws_region" {
  value = var.aws_region
}
