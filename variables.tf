variable "aws_region" {
  type    = string
  default = "eu-west-2"
}

variable "my_ip" {
  type      = string
  default   = "123.123.123.1"
  sensitive = true
}

variable "db_user" {
  type      = string
  sensitive = true
  default   = "admin"
}

variable "db_password" {
  type      = string
  sensitive = true
  default   = "password"
}